#!/bin/bash

echo "*************************** "
echo "** Building jar ***********"
echo "***************************"
docker run --rm  -v $WORKSPACE/java-app:/app -v /home/mehdi/.m2/:/root/.m2/ -w /app maven:3-alpine "$@"
#package
