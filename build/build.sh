#!/bin/bash

cp -f java-app/target/*.jar build

echo "****************************"
echo "** Building Docker Image ***"
echo "****************************"

cd build && docker-compose -f docker-compose.yml build --no-cache
