#!/bin/bash

echo java-app > /tmp/.auth
echo $BUILD_TAG >> /tmp/.auth
echo $USER >> /tmp/.auth
echo $PASS >> /tmp/.auth
address=prod_user@prod_host
echo "sending the auth file..."
scp -i /opt/prod /tmp/.auth $address:/tmp/.auth
echo "sending the publish file..."
scp -i /opt/prod ./deploy/publish $address:/tmp/publish
echo "executing pulish..."
ssh -i /opt/prod $address "/tmp/publish"
